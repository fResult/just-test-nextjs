import React from 'react'
import Head from 'next/head'

import { getAllPostIds, getPostDataById } from '../../lib/posts'
import utilStyles from '../../styles/utils.module.scss'

import Layout from '../../components/layout'
import Date from '../../components/date'

export default function Post({ postData = {} }) {
  // const { title, date, contentHtml } = postData
  return (
    <Layout>
      <Head>
        <title>{postData?.title}</title>
      </Head>
      <h1 className={utilStyles.headingXl}>{postData?.title}</h1>
      <div className={utilStyles.lightText}>
        <Date fontSize={utilStyles.lightText} {...{ dateStr: postData?.date }} />
      </div>
      <div dangerouslySetInnerHTML={{ __html: postData?.contentHtml }} />
    </Layout>
  )
}

export async function getStaticPaths() {
  const paths = getAllPostIds()
  return { paths, fallback: true }
}

export async function getStaticProps({ params }) {
  const postData = await getPostDataById(params.id)
  return { props: { postData: postData || {} } }
}
