import firebase from 'firebase'

const firebaseConfig = {
  apiKey: process.env.API_KEY,
  authDomain: process.env.AUTH_DOMAIN,
  databaseURL: process.env.DATABASE_URL,
  projectId: process.env.PROJECT_ID,
  storageBucket: process.env.STORAGE_BUCKET,
  messagingSenderId: process.env.MESSAGING_SENDER_ID,
  appId: process.env.API_ID
}

try {
  firebase.initializeApp(firebaseConfig)
} catch (err) {
  if (!/already exist/.test(err.message)) {
    console.error('❌ Firebase initialize app error', err.stack)
  }
}

export const firestore = firebase.firestore()
