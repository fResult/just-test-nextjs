import React from 'react'
import { format, parseISO } from 'date-fns'

const Date = ({ dateStr = '1730-12-31' }) => {
  const date = parseISO(dateStr)
  return <time dateTime={dateStr}>{format(date, 'LLLL dd, yyyy')}</time>
}

export default Date
